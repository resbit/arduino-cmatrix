/*
 * Display.hpp
 *
 *  Created on: Feb 16, 2019
 *      Author: ryan
 */

#ifndef DISPLAY_HPP_
#define DISPLAY_HPP_

#include "Matrix.hpp"

#define PICTURE_NULL    250
#define ANIMATION_NULL  251


struct Pixel
{
    // uint8_p (from PGMWrap.h) stores every value into a PROGMEM wrapper
    uint8_p posX;
    uint8_p posY;
    uint8_p red;
    uint8_p green;
    uint8_p blue;
};

typedef Pixel Picture[];        // A picture is a collection of pixels
typedef Picture Animation;      // An animation is also just a collection of pixels
typedef Pixel* Pxlptr;

void drawPicture(const Picture picture, timeMilli delayAfterPicture = 0);
void drawAnimation(const Animation animation, timeMilli frameInterval, timeMilli delayAfterAnimation, bool reverse = false, uint16_t iterations = 1);

#endif /* DISPLAY_HPP_ */
