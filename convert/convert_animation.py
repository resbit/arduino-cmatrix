#!/usr/bin/env python3

import sys

import numpy as np
import imageio

filenamespace = sys.argv[1]  # "EggB/EggB"
numfiles = sys.argv[2]  # 6
defname = "".join("ANIMATION_" + filenamespace.split("/")[0].upper())

backR = sys.argv[3] # 10
backG = sys.argv[4] # 10
backB = sys.argv[5] # 10

RED_CHANNEL = 0
GREEN_CHANNEL = 1
BLUE_CHANNEL = 2
ALPHA_CHANNEL = 3

animation_file = "#ifndef {defname}_HPP\n" \
                 "#define {defname}_HPP\n" \
                 "\n" \
                 "#include \"../Display.hpp\"\n" \
                 "\n" \
                 "PROGMEM const Animation {defname} = {{\n".format(defname=defname)

numFrames = 0

for imgnum in range(1, numfiles+1):
    numPixels = 0

    filename = 'res/{0}.png'.format(filenamespace + str(imgnum))
    image = np.transpose(imageio.imread(filename), axes=(1, 0, 2))

    finImage = ""
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[x, y, 3] != 0:
                finImage += "    {{{x},{y},{r},{g},{b}}},\n".format(x=x, y=y, r=image[x, y, RED_CHANNEL],
                                                                g=image[x, y, GREEN_CHANNEL],
                                                                b=image[x, y, BLUE_CHANNEL])
                numPixels += 1

    finImage += "    {{PICTURE_NULL,{np},{br},{bg},{bb}}},\n\n".format(np=numPixels, br=backR, bg=backG, bb=backB)

    numFrames += 1
    animation_file += finImage

animation_file += "    {{ANIMATION_NULL,{nf},0,0,0}}\n\n}};\n\n#endif\n".format(nf=numFrames)

print(animation_file)

filename = "{name}.hpp".format(name=defname.title())

with open(filename, 'w+') as file:
    file.write(animation_file)
