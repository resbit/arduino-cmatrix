/*
 * Configuration.hpp
 *
 *  Created on: Feb 18, 2019
 *      Author: ryan
 */

#ifndef CONFIGURATION_HPP_
#define CONFIGURATION_HPP_

#include "Matrix.hpp"

//Define RELEASE or DEBUG
#define DEBUG

#define DATA_PIN    3
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

#define BRIGHTNESS  196 // Master Brightness: 0 to 255

const uint8_t kMatrixWidth = 10;
const uint8_t kMatrixHeight = 10;

#endif /* CONFIGURATION_HPP_ */
