/*
 * Setup.hpp
 *
 *  Created on: Feb 16, 2019
 *      Author: ryan
 */

#ifndef SETUP_HPP_
#define SETUP_HPP_

#include "Configuration.hpp"
#include "Matrix.hpp"

#define NUM_LEDS (kMatrixWidth * kMatrixHeight)

uint16_t XY(uint8_t x, uint8_t y);
uint16_t XYsafe(uint8_t x, uint8_t y);

void initLedMatrix(int SerialBaud);
void toggleLED(uint8_t port, timeMilli delayAfterToggle = 0, uint8_t iterations = 0);

#endif /* SETUP_HPP_ */
