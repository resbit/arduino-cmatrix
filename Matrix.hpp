#ifndef _Matrix_H_
#define _Matrix_H_
#include "Arduino.h"

#include "lib/FastLED/FastLED.h"
#include "lib/PGMWrap/PGMWrap.h"
FASTLED_USING_NAMESPACE

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

typedef int timeMilli;

#include "Configuration.hpp"
#include "Setup.hpp"
#include "Display.hpp"

extern CRGB* const leds;


#endif /* _Matrix_H_ */
